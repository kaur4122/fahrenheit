package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
     
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void ExceptionalTest() {
		int temp = -6;
		
		int nav = Fahrenheit.convertFromCelsius(temp);
		System.out.println("The value is Invalid.");
		assertTrue("Invalid",nav<32 || nav>212);
	}
	
	@Test
	public void RegularTest() {
        int temp = 100;
		
		int nav = Fahrenheit.convertFromCelsius(temp);
		System.out.println("The value is Valid.");
		assertTrue("Invalid",nav>32 && nav<212);    
	}
	
	@Test
	public void OutTest() {
        int temp = -59;
		
		int nav = Fahrenheit.convertFromCelsius(temp);
		System.out.println("The value is InValid.");
		assertTrue("Invalid",nav<32 || nav>212);
	}
	
	@Test
	public void InTest() {
        int temp = -1;
        
        
		
		int nav = Fahrenheit.convertFromCelsius(temp);
		System.out.println("The value is INValid.");
		assertTrue("Invalid",nav<32 || nav>212);
	}
	}


